package com.mskill.batch49.repository;

import com.mskill.batch49.entities.IOU;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IOURepository extends JpaRepository<IOU, Integer> {

}
