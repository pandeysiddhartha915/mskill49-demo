package com.mskill.batch49.controllers;

import com.mskill.batch49.services.IOUService;
import com.mskill.batch49.views.iou.IOURequest;
import com.mskill.batch49.views.iou.IOUResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/iou")
public class IOUController {

    @Autowired
    private IOUService iouService;

    @GetMapping("/list")
    public List<IOUResponse> read() {
        return iouService.read();
    }

    @PostMapping("/add")
    public void add(@RequestBody IOURequest iouRequest) {
        iouService.add(iouRequest);
    }

    @PutMapping("/update")
    public void update(@RequestBody IOURequest iouRequest) {
        iouService.update(iouRequest);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam Integer empId) {
        iouService.delete(empId);
    }

}
