package com.mskill.batch49.controllers;

import com.mskill.batch49.services.EmployeeService;
import com.mskill.batch49.views.employee.EmployeeRequest;
import com.mskill.batch49.views.employee.EmployeeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired  //dependency injection
    private EmployeeService employeeService;

    @GetMapping("/list")
    public List<EmployeeResponse> list() {
        return employeeService.read();
//        List<EmployeeResponse> responses = new ArrayList<>();
//        for (EmployeeRequest request : employeeRequests) {
//            responses.add(new EmployeeResponse(request));
//        }
//        return responses;
    }

    @GetMapping("/list1")
    public List<EmployeeResponse> list1() {
        return employeeService.read();
//        return employeeRequests.stream().map(EmployeeResponse::new).collect(Collectors.toList());
    }

    @PostMapping("/add")
    public void add(@RequestBody EmployeeRequest employeeRequest) {
        employeeService.create(employeeRequest);
//        employeeRequests.add(employeeRequest);
    }

    @PutMapping("/update")
    public void update(@RequestBody EmployeeRequest updatedEmployee) {
        employeeService.update(updatedEmployee);
//        for (int i=0; i < employeeRequests.size(); i++) {
//
//            EmployeeRequest request = employeeRequests.get(i);
//            if (updatedEmployee.getEmpId() == request.getEmpId()) {
//                request.setDesignation(updatedEmployee.getDesignation());
//            }
//
//        }
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam int empId) {
        employeeService.delete(empId);
//        for (int i=0; i < employeeRequests.size(); i++) {
//
//            EmployeeRequest request = employeeRequests.get(i);
//            if (empId == request.getEmpId()) {
//                employeeRequests.remove(i);
//            }
//
//        }
    }

}
