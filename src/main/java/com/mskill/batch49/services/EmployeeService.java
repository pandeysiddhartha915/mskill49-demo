package com.mskill.batch49.services;

import com.mskill.batch49.entities.Employee;
import com.mskill.batch49.entities.IOU;
import com.mskill.batch49.repository.EmployeeRepository;
import com.mskill.batch49.repository.IOURepository;
import com.mskill.batch49.views.employee.EmployeeRequest;
import com.mskill.batch49.views.employee.EmployeeResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EmployeeService {

    @Autowired
    private IOURepository iouRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public void create(EmployeeRequest createRequest) {
        Optional<IOU> opIOU = iouRepository.findById(createRequest.getIouId());
        if (opIOU.isPresent()) {
            // INSERT INTO employee() VALUES();
            Employee newEmployee = new Employee(createRequest, opIOU.get());
            employeeRepository.saveAndFlush(newEmployee);
        } else {
            throw new RuntimeException("Invalid IOU Id");
        }
    }

    public List<EmployeeResponse> read() {
        // SELECT * FROM employee;
        List<Employee> employees = employeeRepository.findAll();
        List<EmployeeResponse> responses = new ArrayList<>();
        for (Employee employee : employees) {
            EmployeeResponse response = new EmployeeResponse(employee);
            responses.add(response);
        }
        return responses;
        // return employees.stream().map(EmployeeResponse::new).collect(Collectors.toList());
    }

    public void update(EmployeeRequest updateRequest) {

    }

    public void delete(int empId) {

    }

}
