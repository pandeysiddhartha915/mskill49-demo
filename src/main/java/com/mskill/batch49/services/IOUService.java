package com.mskill.batch49.services;

import com.mskill.batch49.entities.IOU;
import com.mskill.batch49.repository.IOURepository;
import com.mskill.batch49.views.iou.IOURequest;
import com.mskill.batch49.views.iou.IOUResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class IOUService {

//    private static final Logger log = LoggerFactory.getLogger(IOUService.class);

    @Autowired
    private IOURepository iouRepository;

    public List<IOUResponse> read() {
        return iouRepository.findAll().stream().map(IOUResponse::new).collect(Collectors.toList());
    }

    public void add(IOURequest addRequest) {
        IOU newIOU = new IOU(addRequest);
        iouRepository.save(newIOU);
    }

    public void update(IOURequest updateRequest) {
        // SELECT * FROM iou WHERE id=?;
        Optional<IOU> opIOU = iouRepository.findById(updateRequest.getId());
        if (opIOU.isPresent()) {
            IOU iou = opIOU.get();
            iou.update(updateRequest);
//            INSERT INTO table (column_list)
//            VALUES (value_list)
//            ON DUPLICATE KEY UPDATE
//            c1 = v1
            iouRepository.save(iou);
            log.info("Saved the IOU");
        } else {
            log.error("Invalid Id encountetered {}", updateRequest.getId());
            throw new RuntimeException("Invalid Id");
        }
    }

    public void delete(Integer empId) {
        iouRepository.deleteById(empId);
    }

}
