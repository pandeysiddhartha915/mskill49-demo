package com.mskill.batch49.entities;

import com.mskill.batch49.views.employee.EmployeeRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int empId;

    private String name;

    private int experience;

    private String designation;

    @ManyToOne
    private IOU iou;

    public Employee(EmployeeRequest createRequest, IOU parentIOU) {
        this.name = createRequest.getName();
        this.experience = createRequest.getExperience();
        this.designation = createRequest.getDesignation();
        this.iou = parentIOU;
    }
}
