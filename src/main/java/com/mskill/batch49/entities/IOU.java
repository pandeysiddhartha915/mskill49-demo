package com.mskill.batch49.entities;

import com.mskill.batch49.views.iou.IOURequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class IOU {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    @OneToMany(mappedBy = "iou")
    private List<Employee> employees;

    public IOU(IOURequest addRequest) {
        this.name = addRequest.getName();
    }

    public void update(IOURequest updateRequest) {
        if (updateRequest.getName() != null) {
            this.name = updateRequest.getName();
        }
    }
}
