package com.mskill.batch49;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch49Application {

	public static void main(String[] args) {
		SpringApplication.run(Batch49Application.class, args);
	}

}
