package com.mskill.batch49.views.iou;

import com.mskill.batch49.entities.IOU;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IOUResponse {

    private Integer id;
    private String name;

    public IOUResponse(IOU iou) {
        this.id = iou.getId();
        this.name = iou.getName();
    }

}
