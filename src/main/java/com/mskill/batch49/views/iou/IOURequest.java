package com.mskill.batch49.views.iou;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IOURequest {

    private Integer id;
    private String name;

}
