package com.mskill.batch49.views.employee;

import com.mskill.batch49.entities.Employee;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeResponse {

    private int empId;
    private String name;
    private String designation;
//    private String description;
//    private String grade;
    private int experience;


    public EmployeeResponse(Employee employee) {
        this.empId = employee.getEmpId();
        this.experience = employee.getExperience();
        this.name = employee.getName();
        this.designation = employee.getDesignation();
    }
}
