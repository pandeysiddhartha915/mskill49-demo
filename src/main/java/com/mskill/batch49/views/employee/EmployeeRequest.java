package com.mskill.batch49.views.employee;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeRequest {

    private int iouId;
    private int empId;
    private String name;
    private int age;
    private String designation;
    private String description;
    private String grade;
    private int experience;

}
